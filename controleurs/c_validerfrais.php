<?php
include("vues/v_sommairecomp.php");
$action = $_REQUEST['action'];

switch ($action) {
    case 'choisirVisiteur': {
            $lesVisiteurs = $pdo->getLesIdVisiteurs();

            /* Calcul des 6 derniers mois à afficher */
            $lesMois = array();
            for ($i = 0; $i < 6; $i++) {
                $date = strtotime(date("Y-m-d") . "-" . $i . " month");
                $lesMois[date("Ym", $date)] = date("F Y", $date);
            }

            include('vues/v_choisirVisiteur.php');
            break;
        }
    case 'afficherVisiteur': {
            $lesVisiteurs = $pdo->getLesIdVisiteurs();

            /* Calcul des 6 derniers mois à afficher */
            $lesMois = array();
            for ($i = 0; $i < 6; $i++) {
                $date = strtotime(date("Y-m-d") . "-" . $i . " month");
                $lesMois[date("Ym", $date)] = date("F Y", $date);
            }
            include('vues/v_choisirVisiteur.php');

            $leMois = $_REQUEST['lstMois'];
            $idVisiteur = $_REQUEST['lstVisiteurs'];

            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $leMois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $leMois);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $leMois);

            if (empty($lesInfosFicheFrais)) {
                ajouterErreur("Pas de fiche de frais pour ce visiteur ce mois.");
                include("vues/v_erreurs.php");
                break;
            }

            $numAnnee = substr($leMois, 0, 4);
            $numMois = substr($leMois, 4, 2);
            $libEtat = $lesInfosFicheFrais['libEtat'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            $dateModif =  $lesInfosFicheFrais['dateModif'];
            $dateModif =  dateAnglaisVersFrancais($dateModif);

            include('vues/v_afficherVisiteur.php');
            break;
        }
    case 'validerMajFraisForfait': {
            $lesFrais = $_REQUEST['lesFrais'];
            $idVisiteur = $_REQUEST['idVisiteur'];
            $leMois = $_REQUEST['leMois'];
            if (lesQteFraisValides($lesFrais)) {
                $pdo->majFraisForfait($idVisiteur, $leMois, $lesFrais);
            } else {
                ajouterErreur("Les valeurs des frais doivent être numériques");
                include("vues/v_erreurs.php");
            }
            break;
        }
    case 'supprimerFraisHorsForfait': {
            $idFrais = $_REQUEST['idFrais'];
            $idVisiteur = $_REQUEST['idVisiteur'];
            $numMois = $_REQUEST['numMois'];
            $numAnnee = $_REQUEST['numAnnee'];
            $date = $_REQUEST['date'];
            $montant = $_REQUEST['montant'];
            $libelle = $_REQUEST['libelle'];

            if($numMois == 12){
                $numMois = "01";
                $numAnnee = $numAnnee + 1;
            }
            else {
                $numMois = $numMois+1;
            }

            $mois = $numAnnee.$numMois;


            $pdo->supprimerFraisHorsForfait($idFrais);
            if($pdo->estPremierFraisMois($idVisiteur, $mois)){
                $pdo->creeNouveauFicheFrais($idVisiteur, $mois);
            }
            $libelle = "REFUSE : ".$libelle;
            if(strlen($libelle) > 100){
              $libelle = substr($libelle, 0, 100);
            }
            $pdo->creeNouveauFraisHorsForfait($idVisiteur, $mois, $libelle, $date, $montant);
            break;
        }
    case 'reporterFraisHorsForfait': {
            $idFrais = $_REQUEST['idFrais'];
            $idVisiteur = $_REQUEST['idVisiteur'];
            $numMois = $_REQUEST['numMois'];
            $numAnnee = $_REQUEST['numAnnee'];
            $date = $_REQUEST['date'];
            $montant = $_REQUEST['montant'];
            $libelle = $_REQUEST['libelle'];

            if($numMois == 12){
                $numMois = "01";
                $numAnnee = $numAnnee + 1;
            }
            else {
                $numMois = $numMois+1;
            }

            $mois = $numAnnee.$numMois;


            $pdo->supprimerFraisHorsForfait($idFrais);
            if($pdo->estPremierFraisMois($idVisiteur, $mois)){
                $pdo->creeNouveauFicheFrais($idVisiteur, $mois);
            }
            if(strlen($libelle) > 100){
              $libelle = substr($libelle, 0, 100);
            }
            $pdo->creeNouveauFraisHorsForfait($idVisiteur, $mois, $libelle, $date, $montant);
            break;
        }
    case 'validerFicheFrais': {
            $idVisiteur = $_REQUEST['idVisiteur'];
            $numMois = $_REQUEST['numMois'];
            $numAnnee = $_REQUEST['numAnnee'];

            $mois = $numAnnee.$numMois;

            $pdo->majEtatFicheFrais($idVisiteur, $mois, "VA");

            break;
        }
}
