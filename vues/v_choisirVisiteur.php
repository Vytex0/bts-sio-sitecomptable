<div id="contenu">
    <form action="index.php?uc=validerfichefrais&action=afficherVisiteur" method="post">
        <select id="lstVisiteurs" name="lstVisiteurs">
            <?php
            foreach ($lesVisiteurs as $unVisteur) {
            ?>
                <option selected value="<?php echo $unVisteur['id'] ?>"><?php echo $unVisteur['prenom'] . ' ' . $unVisteur['nom'] ?></option>
            <?php
            }
            ?>
        </select>
        <select id="lstMois" name="lstMois">
            <?php foreach ($lesMois as $value => $affiche) {
                echo "<option value=\"" . $value . "\" selected>" . $affiche . '<br/>' . "</option>";
            } ?>
        </select>
        <div class="piedForm">
            <p>
                <input id="ok" type="submit" value="Valider" size="20" />
                <input id="annuler" type="reset" value="Effacer" size="20" />
            </p>
        </div>
    </form>
