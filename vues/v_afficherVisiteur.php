<h3>Fiche de frais du mois <?php echo $numMois . "-" . $numAnnee ?> :
</h3>
<div class="encadre">
    <p>
        Etat : <?php echo $libEtat ?> depuis le <?php echo $dateModif ?> <br> Montant validé : <?php echo $montantValide ?>
    </p>
    <form method="POST" action="index.php?uc=validerfichefrais&action=validerMajFraisForfait">
        <div class="corpsForm">

            <fieldset>
                <legend>Eléments forfaitisés
                </legend>
                <?php
                foreach ($lesFraisForfait as $unFrais) {
                    $idFrais = $unFrais['idfrais'];
                    $libelle = $unFrais['libelle'];
                    $quantite = $unFrais['quantite'];
                ?>
                    <p>
                        <label for="idFrais"><?php echo $libelle ?></label>
                        <input type="text" id="idFrais" name="lesFrais[<?php echo $idFrais ?>]" size="10" maxlength="5" value="<?php echo $quantite ?>">
                    </p>

                <?php
                }
                ?>
            </fieldset>
        </div>
        <input type="text" id="idVisiteur" name="idVisiteur" value="<?php echo $idVisiteur ?>" hidden></input>
        <input type="text" id="leMois" name="leMois" value="<?php echo $leMois ?>" hidden></input>
        <div class="piedForm">
            <p>
                <input id="ok" type="submit" value="Valider" size="20" />
                <input id="annuler" type="reset" value="Effacer" size="20" />
            </p>
        </div>

    </form>
    <table class="listeLegere">
        <caption>Descriptif des éléments hors forfait -<?php echo $nbJustificatifs ?> justificatifs reçus -
        </caption>
        <tr>
            <th class="date">Date</th>
            <th class="libelle">Libellé</th>
            <th class='montant'>Montant</th>
            <th class='action' colspan="2">Actions</th>
        </tr>
        <?php
        foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
            $id = $unFraisHorsForfait['id'];
            $date = $unFraisHorsForfait['date'];
            $libelle = $unFraisHorsForfait['libelle'];
            $montant = $unFraisHorsForfait['montant'];
        ?>
            <tr>
                <td><?php echo $date; ?></td>
                <td><?php echo $libelle; ?></td>
                <td><?php echo $montant; ?></td>
                <td><a href="index.php?uc=validerfichefrais&action=supprimerFraisHorsForfait&idFrais=<?php echo $id; ?>&idVisiteur=<?php echo $idVisiteur; ?>&numMois=<?php echo $numMois; ?>&numAnnee=<?php echo $numAnnee; ?>&date=<?php echo $date; ?>&montant=<?php echo $montant; ?>&libelle=<?php echo $libelle; ?>">Supprimer</a></td>
                <td><a href="index.php?uc=validerfichefrais&action=reporterFraisHorsForfait&idFrais=<?php echo $id; ?>&idVisiteur=<?php echo $idVisiteur; ?>&numMois=<?php echo $numMois; ?>&numAnnee=<?php echo $numAnnee; ?>&date=<?php echo $date; ?>&montant=<?php echo $montant; ?>&libelle=<?php echo $libelle; ?>">Reporter</a></td>
            </tr>
        <?php
        }
        ?>
    </table>
    <form method="POST" action="index.php?uc=validerfichefrais&action=validerFicheFrais">
      <input type="hidden" name="idVisiteur" value="<?php echo $idVisiteur; ?>">
      <input type="hidden" name="numMois" value="<?php echo $numMois; ?>">
      <input type="hidden" name="numAnnee" value="<?php echo $numAnnee; ?>">
      <input type="submit" name="submitFicheFrais" value="Valider la fiche"></input>
    </form>
</div>
</div>
