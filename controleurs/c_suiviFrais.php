<?php
include("vues/v_sommairecomp.php");
$action = $_REQUEST['action'];

switch ($action) {
    case 'selectionnerFicheDeFrais': {
            $fichesFrais = $pdo->getLesInfosFicheFraisValidees();

            include('vues/v_selectionnerFicheDeFrais.php');
            break;
        }
    case 'afficherFicheFrais': {
            $fichesFrais = $pdo->getLesInfosFicheFraisValidees();

            include('vues/v_selectionnerFicheDeFrais.php');

            $leMois = explode("-", $_REQUEST['lstFichesValidees'])[0];
            $idVisiteur = explode("-", $_REQUEST['lstFichesValidees'])[1];

            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $leMois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $leMois);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $leMois);

            if (empty($lesInfosFicheFrais)) {
                ajouterErreur("Pas de fiche de frais pour ce visiteur ce mois.");
                include("vues/v_erreurs.php");
                break;
            }

            $numAnnee = substr($leMois, 0, 4);
            $numMois = substr($leMois, 4, 2);
            $libEtat = $lesInfosFicheFrais['libEtat'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            $dateModif =  $lesInfosFicheFrais['dateModif'];
            $dateModif =  dateAnglaisVersFrancais($dateModif);

            include('vues/v_afficherFicheFrais.php');
            break;
        }
    case 'validerRemboursementFicheFrais': {
            $idVisiteur = $_REQUEST['idVisiteur'];
            $numMois = $_REQUEST['numMois'];
            $numAnnee = $_REQUEST['numAnnee'];

            $fichesFrais = $pdo->majEtatFicheFrais($idVisiteur, $numAnnee.$numMois, "RB");

            break;
        }
}
