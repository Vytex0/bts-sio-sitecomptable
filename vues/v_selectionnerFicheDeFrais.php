<div id="contenu">
    <form action="index.php?uc=suiviFrais&action=afficherFicheFrais" method="post">
        <select id="lstFichesValidees" name="lstFichesValidees">
            <?php
            foreach ($fichesFrais as $frais) {
              $idVisiteur = $frais["idVisiteur"];
              $mois = $frais["mois"];
              $numAnnee = substr($mois, 0, 4);
              $numMois = substr($mois, 4, 2);
              $prenom = $frais["prenom"];
              $nom = $frais["nom"];
            ?>
                <option selected value="<?php echo $mois."-".$idVisiteur; ?>"><?php echo $numMois."/".$numAnnee; ?> - <?php echo $prenom . ' ' . $nom ?></option>
            <?php
            }
            ?>
        </select>
        <div class="piedForm">
            <p>
                <input id="ok" type="submit" value="Valider" size="20" />
            </p>
        </div>
    </form>
